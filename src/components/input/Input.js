import React, {Component} from 'react';

class Input extends Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.filmss.film !== this.props.filmss.film;
    }

    render() {
        return (
            <div>
                <input
                    onChange={e => this.props.change(this.props.filmss.id, e.target.value)}
                    type="text"
                    value={this.props.filmss.film} key={this.props.filmss.id}/>
                <button onClick={e => this.props.remove(this.props.filmss.id)}>DELETE</button>
            </div>
        )
    }
}

export default Input;