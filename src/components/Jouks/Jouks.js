import React from 'react';
import {useEffect} from "react";
import {useState} from "react";

const url = 'https://api.chucknorris.io/jokes/random';

const Jouks = () => {

    const [jokes, setJokes] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const posts = await response.json();
                setJokes([posts]);
            }
        };

        fetchData().catch(e => console.error(e))
    }, []);

    return (
        <div>
            {jokes.map((joke, i) => {
                return <p key={i}>{joke.value}</p>
            })}
        </div>
    );
};

export default Jouks;