import React, {Component} from 'react';
import Input from "../input/Input";

class MainInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            films: [],
            newMovie: [],
        };
    }

    onMainInputChange = () => {
        this.setState({
            newMovie: [
                ...this.state.newMovie, {film: this.state.films, id: Math.random()}
            ]
        })
    };

    changeInputField = (id, movie) => {
        this.setState({
                newMovie: this.state.newMovie.map(f => {
                    if (f.id === id) {
                        return {...f, film: movie}
                    }
                    return f;
                })
            }
        );
    };

    removeInput = (id) => {
        this.setState({
            newMovie: this.state.newMovie.filter(f => f.id !== id)
        })
    };

    render() {
        return (
            <div>
                <label>
                    <input onChange={e => this.setState({films: e.target.value})} type="text" value={this.state.films}/>
                </label>
                <button onClick={this.onMainInputChange}>Add</button>
                <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                    {this.state.newMovie.map((movie, index) => {
                        return <Input remove={this.removeInput} change={this.changeInputField} filmss={this.state.newMovie[index]}/>
                    })}
                </div>
            </div>
        );
    }
}

export default MainInput;