import './App.css';
import MainInput from "./components/MainInput/MainInput";
import Jouks from "./components/Jouks/Jouks";

function App() {
    return (
        <div className="App">
            <MainInput/>
            <Jouks/>
        </div>
    );
}

export default App;
